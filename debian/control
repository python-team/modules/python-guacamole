Source: python-guacamole
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Zygmunt Krynicki <zygmunt.krynicki@canonical.com>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python-all,
               python-mock,
               python-setuptools,
               python-unittest2,
               python3-all,
               python3-doc,
               python3-setuptools,
               python3-sphinx
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/python-guacamole.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-guacamole
Homepage: https://github.com/zyga/guacamole/

Package: python-guacamole
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Suggests: python-guacamole-doc
Description: framework for creating command line applications (Python 2)
 Guacamole is a flexible, modular system for creating command line
 applications.  Guacamole comes with built-in support for writing command line
 applications that integrate well with the running system. A short list of
 supported features (ingredients) includes:
 .
  - handling flat and hierarchical commands
  - hassle-free crash detection
  - hassle-free logging
  - internationalization and localization
 .
 The guacamole ingredient system allows for third party add-ons.
 .
 This package contains the Python 2.x version of the library

Package: python-guacamole-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: framework for creating command line applications (documentation)
 Guacamole is a flexible, modular system for creating command line
 applications.  Guacamole comes with built-in support for writing command line
 applications that integrate well with the running system. A short list of
 supported features (ingredients) includes:
 .
  - handling flat and hierarchical commands
  - hassle-free crash detection
  - hassle-free logging
  - internationalization and localization
 .
 The guacamole ingredient system allows for third party add-ons.
 .
 This package contains the HTML documentation and example applications.

Package: python3-guacamole
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-guacamole-doc
Description: framework for creating command line applications (Python 3)
 Guacamole is a flexible, modular system for creating command line
 applications.  Guacamole comes with built-in support for writing command line
 applications that integrate well with the running system. A short list of
 supported features (ingredients) includes:
 .
  - handling flat and hierarchical commands
  - hassle-free crash detection
  - hassle-free logging
  - internationalization and localization
 .
 The guacamole ingredient system allows for third party add-ons.
 .
 This package contains the Python 3.x version of the library
